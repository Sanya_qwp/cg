﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Affin{
    public partial class Form1 : Form{
        public Form1(){InitializeComponent();}
        static Graphics h;
        List<Point> pointXY = new List<Point> { }; double[,] matrix = new double[3, 3];
        double[,] tempMatrix = new double[100, 3]; double[,] finalMatrix = new double[100, 3];
        double[,] swiftToCenter = new double[100, 3]; double[,] swiftBack = new Double[100, 3];
        double[,] matrixInCenter = new Double[100, 3]; double[,] matrixMultiply = new Double[100, 3];
        int count = 0; float avgX = 0, avgY = 0;
        //Functions for init
        void drawCoordsPlate(){
            Pen pen = new Pen(Color.Black, 1); Point[] points = new Point[4];
            h = pictureBox1.CreateGraphics(); h.Clear(Color.White);
            points[0] = new Point(0, pictureBox1.Height / 2); points[1] = new Point(pictureBox1.Width, pictureBox1.Height / 2);
            points[2] = new Point(pictureBox1.Width / 2, pictureBox1.Height); points[3] = new Point(pictureBox1.Width / 2, 0);
            h.DrawLine(pen, points[0], points[1]);
            h.DrawLine(pen, points[2], points[3]);
        }
        void drawFigure(){
            h = pictureBox1.CreateGraphics();
            Pen pen = new Pen(Color.Red, 1);
            for (int i = 0; i < count - 1; ++i){
                h.DrawLine(pen, pointXY[i], pointXY[i + 1]);
            }
            h.DrawLine(pen, pointXY[count - 1], pointXY[0]);
            avgX = pictureBox1.Width / 2; avgY = pictureBox1.Height / 2;
        }
        //Methods for init
        private void renderButton_Click(object sender, EventArgs e){
            drawCoordsPlate();
        }
        private void pictureBox1_MouseClick(object sender, MouseEventArgs e){
            int x = e.X; int y = e.Y;
            //setCoords(x, y);
            Pen pen = new Pen(Color.Blue, 5);
            pointXY.Add(new Point(x, y));
            h = pictureBox1.CreateGraphics(); h.DrawRectangle(pen, x, y, 1, 1);
            ++count;
        }
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e){
            Xlabel.Text = "X = " + e.X.ToString();
            Ylabel.Text = "Y = " + e.Y.ToString(); 
        }
        private void drawFigureButton_Click(object sender, EventArgs e){
            drawFigure();
        }
        private void pointsReset_Click(object sender, EventArgs e){
            count = 0; pointXY.Clear();
            h = pictureBox1.CreateGraphics(); h.Clear(Color.White);
            drawCoordsPlate();
            for (int i = 0; i < count; ++i){
                for (int j = 0; j < 3; ++j){
                    tempMatrix[i, j] = 0; matrixInCenter[i, j] = 0; matrixMultiply[i, j] = 0; finalMatrix[i, j] = 0;
                }
            }
        }
        //functions for changes        
        void pointToMatrix(){
            for (int i = 0; i < count; ++i){
                tempMatrix[i, 0] = pointXY[i].X;
                tempMatrix[i, 1] = pointXY[i].Y;
                tempMatrix[i, 2] = 1;
            }
        }
        void goToCenter(){
            swiftToCenter[0, 0] = 1; swiftToCenter[0, 1] = 0; swiftToCenter[0, 2] = 0;
            swiftToCenter[1, 0] = 0; swiftToCenter[1, 1] = 1; swiftToCenter[1, 2] = 0;
            swiftToCenter[2, 0] = -Convert.ToDouble(avgX); ; swiftToCenter[2, 1] = -Convert.ToDouble(avgY); swiftToCenter[2, 2] = 1;
            for (int i = 0; i < count; ++i){
                for (int j = 0; j < 3; ++j){
                    for (int k = 0; k < 3; ++k){
                        matrixInCenter[i, j] += tempMatrix[i, k] * swiftToCenter[k, j];
                    }
                }
            }
        }
        void multMatrix(){
            for (int i = 0; i < count; ++i){
                for (int j = 0; j < 3; ++j){
                    for (int k = 0; k < 3; ++k){
                        matrixMultiply[i, j] += matrixInCenter[i, k] * matrix[k, j];
                    }
                }
            }
        }
        void goBack(){
            swiftBack[0, 0] = 1; swiftBack[0, 1] = 0; swiftBack[0, 2] = 0;
            swiftBack[1, 0] = 0; swiftBack[1, 1] = 1; swiftBack[1, 2] = 0;
            swiftBack[2, 0] = Convert.ToDouble(avgX); swiftBack[2, 1] = Convert.ToDouble(avgY); swiftBack[2, 2] = 1;
            for (int i = 0; i < count; ++i){
                for (int j = 0; j < 3; ++j){
                    for (int k = 0; k < 3; ++k){
                        finalMatrix[i, j] += matrixMultiply[i, k] * swiftBack[k, j];
                    }
                }
            }
        }
        void matrixToPoint(){
            pointXY.Clear();
            for (int i = 0; i < count; ++i){
                pointXY.Add(new Point(Convert.ToInt32(finalMatrix[i, 0]), Convert.ToInt32(finalMatrix[i, 1])));
                for (int j = 0; j < 3; ++j){
                    tempMatrix[i, j] = 0; matrixInCenter[i, j] = 0; matrixMultiply[i, j] = 0; finalMatrix[i, j] = 0;
                }
            }
        }
        //Methods for changes
        private void rotateButton_Click(object sender, EventArgs e){
            int inputData = Convert.ToInt32(rotateInput.Text);
            matrix[0, 0] = Math.Cos(inputData); matrix[0, 1] = Math.Sin(inputData); matrix[0, 2] = 0;
            matrix[1, 0] = -Math.Sin(inputData); matrix[1, 1] = Math.Cos(inputData); matrix[1, 2] = 0;
            matrix[2, 0] = 0; matrix[2, 1] = 0; matrix[2, 2] = 1;  
            pointToMatrix(); goToCenter(); multMatrix(); goBack(); matrixToPoint(); drawCoordsPlate(); drawFigure();
        }
        private void zoomButton_Click(object sender, EventArgs e){
            matrix[0, 0] = Convert.ToDouble(zoomXInput.Text); matrix[0, 1] = 0; matrix[0, 2] = 0; 
            matrix[1, 0] = 0; matrix[1, 1] = Convert.ToDouble(zoomYInput.Text); matrix[1, 2] = 0; 
            matrix[2, 0] = 0; matrix[2, 1] = 0; matrix[2, 2] = 1;                                 
            pointToMatrix(); goToCenter(); multMatrix(); goBack(); matrixToPoint(); drawCoordsPlate(); drawFigure();
        }
        private void swiftButton_Click(object sender, EventArgs e){
            matrix[0, 0] = 1; matrix[0, 1] = 0; matrix[0, 2] = 0;
            matrix[1, 0] = 0; matrix[1, 1] = 1; matrix[1, 2] = 0;
            matrix[2, 0] = Convert.ToDouble(swiftXInput.Text); matrix[2, 1] = Convert.ToDouble(swiftYInput.Text); matrix[2, 2] = 1;
            pointToMatrix(); goToCenter(); multMatrix(); goBack(); matrixToPoint(); drawCoordsPlate(); drawFigure();
        }
        private void hreflectButton_Click(object sender, EventArgs e){
            matrix[0, 0] = -1; matrix[0, 1] = 0; matrix[0, 2] = 0;
            matrix[1, 0] = 0; matrix[1, 1] = 1; matrix[1, 2] = 0;
            matrix[2, 0] = 0; matrix[2, 1] = 0; matrix[2, 2] = 1;
            pointToMatrix(); goToCenter(); multMatrix(); goBack(); matrixToPoint(); drawCoordsPlate(); drawFigure();
        }
        private void vreflectButton_Click(object sender, EventArgs e){
            matrix[0, 0] = 1; matrix[0, 1] = 0; matrix[0, 2] = 0;
            matrix[1, 0] = 0; matrix[1, 1] = -1; matrix[1, 2] = 0;
            matrix[2, 0] = 0; matrix[2, 1] = 0; matrix[2, 2] = 1;
            pointToMatrix(); goToCenter(); multMatrix(); goBack(); matrixToPoint(); drawCoordsPlate(); drawFigure();
        }
    }
}