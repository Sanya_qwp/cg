﻿namespace Affin
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.renderButton = new System.Windows.Forms.Button();
            this.Xlabel = new System.Windows.Forms.Label();
            this.Ylabel = new System.Windows.Forms.Label();
            this.drawFigureButton = new System.Windows.Forms.Button();
            this.pointsReset = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.swiftButton = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.swiftYInput = new System.Windows.Forms.TextBox();
            this.swiftXInput = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.vreflectButton = new System.Windows.Forms.Button();
            this.hreflectButton = new System.Windows.Forms.Button();
            this.zoomButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.zoomYInput = new System.Windows.Forms.TextBox();
            this.zoomXInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.rotateButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.rotateInput = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox1.Location = new System.Drawing.Point(161, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(630, 430);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // renderButton
            // 
            this.renderButton.Location = new System.Drawing.Point(6, 19);
            this.renderButton.Name = "renderButton";
            this.renderButton.Size = new System.Drawing.Size(131, 23);
            this.renderButton.TabIndex = 2;
            this.renderButton.Text = "Draw plate";
            this.renderButton.UseVisualStyleBackColor = true;
            this.renderButton.Click += new System.EventHandler(this.renderButton_Click);
            // 
            // Xlabel
            // 
            this.Xlabel.AutoSize = true;
            this.Xlabel.Location = new System.Drawing.Point(6, 307);
            this.Xlabel.Name = "Xlabel";
            this.Xlabel.Size = new System.Drawing.Size(14, 13);
            this.Xlabel.TabIndex = 3;
            this.Xlabel.Text = "X";
            // 
            // Ylabel
            // 
            this.Ylabel.AutoSize = true;
            this.Ylabel.Location = new System.Drawing.Point(48, 307);
            this.Ylabel.Name = "Ylabel";
            this.Ylabel.Size = new System.Drawing.Size(14, 13);
            this.Ylabel.TabIndex = 4;
            this.Ylabel.Text = "Y";
            // 
            // drawFigureButton
            // 
            this.drawFigureButton.Location = new System.Drawing.Point(6, 48);
            this.drawFigureButton.Name = "drawFigureButton";
            this.drawFigureButton.Size = new System.Drawing.Size(131, 23);
            this.drawFigureButton.TabIndex = 5;
            this.drawFigureButton.Text = "Draw figure";
            this.drawFigureButton.UseVisualStyleBackColor = true;
            this.drawFigureButton.Click += new System.EventHandler(this.drawFigureButton_Click);
            // 
            // pointsReset
            // 
            this.pointsReset.Location = new System.Drawing.Point(6, 77);
            this.pointsReset.Name = "pointsReset";
            this.pointsReset.Size = new System.Drawing.Size(131, 23);
            this.pointsReset.TabIndex = 6;
            this.pointsReset.Text = "Reset array";
            this.pointsReset.UseVisualStyleBackColor = true;
            this.pointsReset.Click += new System.EventHandler(this.pointsReset_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.renderButton);
            this.groupBox1.Controls.Add(this.pointsReset);
            this.groupBox1.Controls.Add(this.drawFigureButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(143, 108);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Render";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.swiftButton);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.swiftYInput);
            this.groupBox2.Controls.Add(this.swiftXInput);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.vreflectButton);
            this.groupBox2.Controls.Add(this.hreflectButton);
            this.groupBox2.Controls.Add(this.zoomButton);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.zoomYInput);
            this.groupBox2.Controls.Add(this.zoomXInput);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.rotateButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.Ylabel);
            this.groupBox2.Controls.Add(this.rotateInput);
            this.groupBox2.Controls.Add(this.Xlabel);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(12, 118);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(143, 323);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Changes";
            // 
            // swiftButton
            // 
            this.swiftButton.Location = new System.Drawing.Point(6, 252);
            this.swiftButton.Name = "swiftButton";
            this.swiftButton.Size = new System.Drawing.Size(131, 23);
            this.swiftButton.TabIndex = 16;
            this.swiftButton.Text = "Swift";
            this.swiftButton.UseVisualStyleBackColor = true;
            this.swiftButton.Click += new System.EventHandler(this.swiftButton_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "dx, dy";
            // 
            // swiftYInput
            // 
            this.swiftYInput.Location = new System.Drawing.Point(48, 226);
            this.swiftYInput.Name = "swiftYInput";
            this.swiftYInput.Size = new System.Drawing.Size(36, 20);
            this.swiftYInput.TabIndex = 14;
            this.swiftYInput.Text = "0";
            // 
            // swiftXInput
            // 
            this.swiftXInput.Location = new System.Drawing.Point(6, 226);
            this.swiftXInput.Name = "swiftXInput";
            this.swiftXInput.Size = new System.Drawing.Size(36, 20);
            this.swiftXInput.TabIndex = 13;
            this.swiftXInput.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 210);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Swift";
            // 
            // vreflectButton
            // 
            this.vreflectButton.Location = new System.Drawing.Point(6, 184);
            this.vreflectButton.Name = "vreflectButton";
            this.vreflectButton.Size = new System.Drawing.Size(131, 23);
            this.vreflectButton.TabIndex = 11;
            this.vreflectButton.Text = "Vertical reflect";
            this.vreflectButton.UseVisualStyleBackColor = true;
            this.vreflectButton.Click += new System.EventHandler(this.vreflectButton_Click);
            // 
            // hreflectButton
            // 
            this.hreflectButton.Location = new System.Drawing.Point(6, 155);
            this.hreflectButton.Name = "hreflectButton";
            this.hreflectButton.Size = new System.Drawing.Size(131, 23);
            this.hreflectButton.TabIndex = 10;
            this.hreflectButton.Text = "Horizontal reflect";
            this.hreflectButton.UseVisualStyleBackColor = true;
            this.hreflectButton.Click += new System.EventHandler(this.hreflectButton_Click);
            // 
            // zoomButton
            // 
            this.zoomButton.Location = new System.Drawing.Point(6, 126);
            this.zoomButton.Name = "zoomButton";
            this.zoomButton.Size = new System.Drawing.Size(131, 23);
            this.zoomButton.TabIndex = 9;
            this.zoomButton.Text = "Zoom";
            this.zoomButton.UseVisualStyleBackColor = true;
            this.zoomButton.Click += new System.EventHandler(this.zoomButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(90, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "dx, dy";
            // 
            // zoomYInput
            // 
            this.zoomYInput.Location = new System.Drawing.Point(48, 100);
            this.zoomYInput.Name = "zoomYInput";
            this.zoomYInput.Size = new System.Drawing.Size(36, 20);
            this.zoomYInput.TabIndex = 7;
            this.zoomYInput.Text = "1";
            // 
            // zoomXInput
            // 
            this.zoomXInput.Location = new System.Drawing.Point(6, 100);
            this.zoomXInput.Name = "zoomXInput";
            this.zoomXInput.Size = new System.Drawing.Size(36, 20);
            this.zoomXInput.TabIndex = 6;
            this.zoomXInput.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Zoom";
            // 
            // rotateButton
            // 
            this.rotateButton.Location = new System.Drawing.Point(6, 58);
            this.rotateButton.Name = "rotateButton";
            this.rotateButton.Size = new System.Drawing.Size(131, 23);
            this.rotateButton.TabIndex = 3;
            this.rotateButton.Text = "Rotate";
            this.rotateButton.UseVisualStyleBackColor = true;
            this.rotateButton.Click += new System.EventHandler(this.rotateButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(48, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "degrees";
            // 
            // rotateInput
            // 
            this.rotateInput.Location = new System.Drawing.Point(6, 32);
            this.rotateInput.Name = "rotateInput";
            this.rotateInput.Size = new System.Drawing.Size(36, 20);
            this.rotateInput.TabIndex = 1;
            this.rotateInput.Text = "90";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Rotate";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(803, 453);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "2d_affin";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button renderButton;
        private System.Windows.Forms.Label Xlabel;
        private System.Windows.Forms.Label Ylabel;
        private System.Windows.Forms.Button drawFigureButton;
        private System.Windows.Forms.Button pointsReset;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button rotateButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox rotateInput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox zoomYInput;
        private System.Windows.Forms.TextBox zoomXInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button zoomButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button vreflectButton;
        private System.Windows.Forms.Button hreflectButton;
        private System.Windows.Forms.Button swiftButton;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox swiftYInput;
        private System.Windows.Forms.TextBox swiftXInput;
    }
}

