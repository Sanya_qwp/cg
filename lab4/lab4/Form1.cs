﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.FreeGlut;
using Tao.OpenGl;
using Tao.Platform.Windows;
namespace lab4{
    public partial class Form1 : Form{
        public Form1()
        {
            InitializeComponent();
        }
        /*
             * Реализуйте программу, позволяющую выполнять композицию
             * преобразований над четырехгранной пирамидой: поворота относительно
             * различных плоскостей и аксонометрическое триметрическое проецирование.
        */
        static Graphics h; float[,] fC = new float[5, 4]; float[,] cPP = new float[4, 2]; float avgx, avgy, avgz;
        private void drawCoordPlates_Click(object sender, EventArgs e)
        {
            drawCoordsPlate();
             Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); Gl.glLoadIdentity(); Gl.glColor3f(1.0f, 0, 0); Gl.glPushMatrix(); Gl.glTranslated(0,0,-6); Gl.glRotated(45, 1, 1, 0); FreeGLUT Glut.glutWireSphere(2, 32, 32); Gl.glPopMatrix(); Gl.glFlush(); AnT.Invalidate();
        }
        private void drawFigure_Click(object sender, EventArgs e){
            //fc: X Y Z | 1
            fC[0, 0] = pictureBox1.Width / 2; fC[0, 1] = pictureBox1.Height / 2; fC[0, 2] = 1; fC[0, 3] = 1;
            fC[1, 0] = pictureBox1.Width * 5 / 6; fC[1, 1] = pictureBox1.Height / 2; fC[1, 2] = 1; fC[1, 3] = 1;
            fC[3, 0] = pictureBox1.Width / 2; fC[3, 1] = pictureBox1.Height / 2; fC[3, 2] = 200; fC[3, 3] = 1;
            fC[2, 0] = pictureBox1.Width * 5 / 6; fC[2, 1] = pictureBox1.Height / 2; fC[2, 2] = 200; fC[2, 3] = 1;
            fC[4, 0] = pictureBox1.Width * 2 / 3; fC[4, 1] = pictureBox1.Height / 6; fC[4, 2] = 100; fC[4, 3] = 1;
            drawFigureFunc();
        }
        void drawCoordsPlate()
        {
            Pen pen = new Pen(Color.Black, 1); h = pictureBox1.CreateGraphics(); h.Clear(Color.White);
            cPP[0, 0] = pictureBox1.Width / 2; cPP[0, 1] = pictureBox1.Height / 2; //CENTER
            cPP[1, 0] = pictureBox1.Width; cPP[1, 1] = pictureBox1.Height / 2; //X
            cPP[2, 0] = pictureBox1.Width / 2; cPP[2, 1] = 0; //Y
            cPP[3, 0] = 0; cPP[3, 1] = pictureBox1.Height; //Z
            h.DrawLine(pen, cPP[0, 0], cPP[0, 1], cPP[1, 0], cPP[1, 1]);
            h.DrawLine(pen, cPP[0, 0], cPP[0, 1], cPP[2, 0], cPP[2, 1]);
            h.DrawLine(pen, cPP[0, 0], cPP[0, 1], cPP[3, 0], cPP[3, 1]);
        }
        void drawFigureFunc(){
            drawCoordsPlate();
            Pen pen = new Pen(Color.Red, 2);
            h.DrawLine(pen, fC[0, 0], fC[0, 1], fC[1, 0], fC[1, 1]); //Грань 1
            h.DrawLine(pen, fC[1, 0], fC[1, 1], fC[2, 0], fC[2, 1]); //Грань 2
            h.DrawLine(pen, fC[2, 0], fC[2, 1], fC[3, 0], fC[3, 1]); //Грань 3
            h.DrawLine(pen, fC[3, 0], fC[3, 1], fC[0, 0], fC[0, 1]); //Замкнуть ряды
            h.DrawLine(pen, fC[0, 0], fC[0, 1], fC[4, 0], fC[4, 1]); //Ребро первой грани к высоте
            h.DrawLine(pen, fC[1, 0], fC[1, 1], fC[4, 0], fC[4, 1]); //Ребро второй грани к высоте
            h.DrawLine(pen, fC[2, 0], fC[2, 1], fC[4, 0], fC[4, 1]); //Ребро третьей грани к высоте
            h.DrawLine(pen, fC[3, 0], fC[3, 1], fC[4, 0], fC[4, 1]); //Ребро четвёртой грани к высоте
        }
        void multMatrix(float[,] tempMatrix){
            float[,] finalMatrix = new float[5, 4];
            for (int i = 0; i < 5; ++i){
                for (int j = 0; j < 4; ++j){
                    for (int k = 0; k < 4; ++k){
                        finalMatrix[i, j] += fC[i, k] * tempMatrix[k, j];
                    }
                }
            }
            for (int i = 0; i < 5; ++i){
                for (int j = 0; j < 4; ++j){
                    fC[i, j] = finalMatrix[i, j];
                }
            }
        }
        void goToCenter(){
            avgx = pictureBox1.Width / 2; avgy = pictureBox1.Height / 2; avgz = 0;
            float[,] tempMatrix = new float[,]{
                {1, 0, 0, 0 },
                {0, 1, 0 ,0 },
                {0, 0, 1, 0 },
                {-avgx, -avgy, -avgz, 1 }
            };
            multMatrix(tempMatrix);
        }
        void goBack(){
            float[,] tempMatrix = new float[,]{
                {1, 0, 0, 0 },
                {0, 1, 0 ,0 },
                {0, 0, 1, 0 },
                {avgx, avgy, avgz, 1 }
            };
            multMatrix(tempMatrix);
        }   
        private void rotateX_Click(object sender, EventArgs e){
            int deg = Convert.ToInt32(degreesSet.Text);
            /*
            float[,] rX = new float[,]{
                {1, 0, 0, 0 },
                {0, (float)Math.Cos(deg*Math.PI/180), (float)Math.Sin(deg*Math.PI/180), 0 },
                {0, -(float)Math.Sin(deg*Math.PI/180), (float)Math.Cos(deg*Math.PI/180), 0 },
                {0, 0, 0, 1 }
            };
            */
            float[,] rX = new float[,]{
                {0.926f, 0.134f, 0, 0 },
                {0, 0.935f, 0, 0 },
                {0.378f, -0.327f, 0, 0 },
                {0, 0, 0, 1 }
            };
            
            goToCenter(); multMatrix(rX); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void rotateY_Click(object sender, EventArgs e){
            int deg = Convert.ToInt32(degreesSet.Text);
            float[,] rY = new float[,]{
                {(float)Math.Cos(deg*Math.PI/180), 0, -(float)Math.Sin(deg*Math.PI/180), 0 },
                {0, 1, 0, 0 },
                {(float)Math.Sin(deg*Math.PI/180), 0, (float)Math.Cos(deg*Math.PI/180), 0 },
                {0, 0, 0, 1 }
            };
            goToCenter(); multMatrix(rY); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void reflectXButton_Click(object sender, EventArgs e){
            float[,] rX = new float[,]{
                {-1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            };
            goToCenter(); multMatrix(rX); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void reflectYButton_Click(object sender, EventArgs e){
            float[,] rY = new float[,]{
                {1, 0, 0, 0},
                {0, -1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1}
            };
            goToCenter(); multMatrix(rY); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void reflectZButton_Click(object sender, EventArgs e){
            float[,] rZ = new float[,]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, -1, 0},
                {0, 0, 0, 1}
            };
            goToCenter(); multMatrix(rZ); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void rotateZ_Click(object sender, EventArgs e){
            int deg = Convert.ToInt32(degreesSet.Text);
            float[,] rZ = new float[,]{
                {(float)Math.Cos(deg*Math.PI/180), (float)Math.Sin(deg*Math.PI/180), 0, 0 },
                {-(float)Math.Sin(deg*Math.PI/180), (float)Math.Cos(deg*Math.PI/180), 0, 0 },
                {0, 0, 1, 0 }, 
                {0, 0, 0, 1 }
            };
            goToCenter(); multMatrix(rZ); goBack();drawCoordsPlate(); drawFigureFunc();
        }
        private void zoomButton_Click(object sender, EventArgs e)
        {
            float zX = float.Parse(xZoom.Text);float zY = float.Parse(yZoom.Text);float zZ = float.Parse(zZoom.Text);
            float[,] zoomMatrix = new float[,]{
                {zX, 0, 0, 0 },
                {0, zY, 0, 0 },
                {0, 0, zZ, 0 },
                {0, 0, 0, 1 }
            };
            goToCenter(); multMatrix(zoomMatrix); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
        private void moveButton_Click(object sender, EventArgs e){
            int mX = Convert.ToInt32(xMove.Text); int mY = Convert.ToInt32(yMove.Text); int mZ = Convert.ToInt32(zMove.Text);
            float[,] mM = new float[,]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {mX, mY, mZ, 1}
            };
            goToCenter(); multMatrix(mM); goBack(); drawCoordsPlate(); drawFigureFunc();
        }
    }
}