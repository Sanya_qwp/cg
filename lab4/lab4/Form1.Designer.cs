﻿namespace lab4
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.drawCoordPlates = new System.Windows.Forms.Button();
            this.drawFigure = new System.Windows.Forms.Button();
            this.degreesSet = new System.Windows.Forms.TextBox();
            this.rotateX = new System.Windows.Forms.Button();
            this.rotateY = new System.Windows.Forms.Button();
            this.rotateZ = new System.Windows.Forms.Button();
            this.xZoom = new System.Windows.Forms.TextBox();
            this.yZoom = new System.Windows.Forms.TextBox();
            this.zZoom = new System.Windows.Forms.TextBox();
            this.zoomButton = new System.Windows.Forms.Button();
            this.reflectXButton = new System.Windows.Forms.Button();
            this.reflectYButton = new System.Windows.Forms.Button();
            this.reflectZButton = new System.Windows.Forms.Button();
            this.xMove = new System.Windows.Forms.TextBox();
            this.yMove = new System.Windows.Forms.TextBox();
            this.zMove = new System.Windows.Forms.TextBox();
            this.moveButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.Location = new System.Drawing.Point(158, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(575, 411);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // drawCoordPlates
            // 
            this.drawCoordPlates.Location = new System.Drawing.Point(12, 12);
            this.drawCoordPlates.Name = "drawCoordPlates";
            this.drawCoordPlates.Size = new System.Drawing.Size(140, 23);
            this.drawCoordPlates.TabIndex = 1;
            this.drawCoordPlates.Text = "Draw Plates";
            this.drawCoordPlates.UseVisualStyleBackColor = true;
            this.drawCoordPlates.Click += new System.EventHandler(this.drawCoordPlates_Click);
            // 
            // drawFigure
            // 
            this.drawFigure.Location = new System.Drawing.Point(12, 41);
            this.drawFigure.Name = "drawFigure";
            this.drawFigure.Size = new System.Drawing.Size(140, 23);
            this.drawFigure.TabIndex = 2;
            this.drawFigure.Text = "Draw Figure";
            this.drawFigure.UseVisualStyleBackColor = true;
            this.drawFigure.Click += new System.EventHandler(this.drawFigure_Click);
            // 
            // degreesSet
            // 
            this.degreesSet.Location = new System.Drawing.Point(12, 70);
            this.degreesSet.Name = "degreesSet";
            this.degreesSet.Size = new System.Drawing.Size(140, 20);
            this.degreesSet.TabIndex = 3;
            this.degreesSet.Text = "45";
            // 
            // rotateX
            // 
            this.rotateX.Location = new System.Drawing.Point(12, 96);
            this.rotateX.Name = "rotateX";
            this.rotateX.Size = new System.Drawing.Size(140, 23);
            this.rotateX.TabIndex = 4;
            this.rotateX.Text = "Rotate X";
            this.rotateX.UseVisualStyleBackColor = true;
            this.rotateX.Click += new System.EventHandler(this.rotateX_Click);
            // 
            // rotateY
            // 
            this.rotateY.Location = new System.Drawing.Point(12, 125);
            this.rotateY.Name = "rotateY";
            this.rotateY.Size = new System.Drawing.Size(140, 23);
            this.rotateY.TabIndex = 5;
            this.rotateY.Text = "Rotate Y";
            this.rotateY.UseVisualStyleBackColor = true;
            this.rotateY.Click += new System.EventHandler(this.rotateY_Click);
            // 
            // rotateZ
            // 
            this.rotateZ.Location = new System.Drawing.Point(12, 154);
            this.rotateZ.Name = "rotateZ";
            this.rotateZ.Size = new System.Drawing.Size(140, 23);
            this.rotateZ.TabIndex = 6;
            this.rotateZ.Text = "Rotate Z";
            this.rotateZ.UseVisualStyleBackColor = true;
            this.rotateZ.Click += new System.EventHandler(this.rotateZ_Click);
            // 
            // xZoom
            // 
            this.xZoom.Location = new System.Drawing.Point(12, 183);
            this.xZoom.Name = "xZoom";
            this.xZoom.Size = new System.Drawing.Size(42, 20);
            this.xZoom.TabIndex = 7;
            this.xZoom.Text = "1";
            // 
            // yZoom
            // 
            this.yZoom.Location = new System.Drawing.Point(60, 183);
            this.yZoom.Name = "yZoom";
            this.yZoom.Size = new System.Drawing.Size(42, 20);
            this.yZoom.TabIndex = 8;
            this.yZoom.Text = "1";
            // 
            // zZoom
            // 
            this.zZoom.Location = new System.Drawing.Point(108, 183);
            this.zZoom.Name = "zZoom";
            this.zZoom.Size = new System.Drawing.Size(42, 20);
            this.zZoom.TabIndex = 9;
            this.zZoom.Text = "1";
            // 
            // zoomButton
            // 
            this.zoomButton.Location = new System.Drawing.Point(12, 209);
            this.zoomButton.Name = "zoomButton";
            this.zoomButton.Size = new System.Drawing.Size(138, 23);
            this.zoomButton.TabIndex = 10;
            this.zoomButton.Text = "Zoom";
            this.zoomButton.UseVisualStyleBackColor = true;
            this.zoomButton.Click += new System.EventHandler(this.zoomButton_Click);
            // 
            // reflectXButton
            // 
            this.reflectXButton.Location = new System.Drawing.Point(12, 238);
            this.reflectXButton.Name = "reflectXButton";
            this.reflectXButton.Size = new System.Drawing.Size(138, 23);
            this.reflectXButton.TabIndex = 11;
            this.reflectXButton.Text = "Reflect X";
            this.reflectXButton.UseVisualStyleBackColor = true;
            this.reflectXButton.Click += new System.EventHandler(this.reflectXButton_Click);
            // 
            // reflectYButton
            // 
            this.reflectYButton.Location = new System.Drawing.Point(12, 267);
            this.reflectYButton.Name = "reflectYButton";
            this.reflectYButton.Size = new System.Drawing.Size(138, 23);
            this.reflectYButton.TabIndex = 12;
            this.reflectYButton.Text = "Reflect Y";
            this.reflectYButton.UseVisualStyleBackColor = true;
            this.reflectYButton.Click += new System.EventHandler(this.reflectYButton_Click);
            // 
            // reflectZButton
            // 
            this.reflectZButton.Location = new System.Drawing.Point(12, 296);
            this.reflectZButton.Name = "reflectZButton";
            this.reflectZButton.Size = new System.Drawing.Size(138, 23);
            this.reflectZButton.TabIndex = 13;
            this.reflectZButton.Text = "Reflect Z";
            this.reflectZButton.UseVisualStyleBackColor = true;
            this.reflectZButton.Click += new System.EventHandler(this.reflectZButton_Click);
            // 
            // xMove
            // 
            this.xMove.Location = new System.Drawing.Point(12, 325);
            this.xMove.Name = "xMove";
            this.xMove.Size = new System.Drawing.Size(42, 20);
            this.xMove.TabIndex = 14;
            this.xMove.Text = "0";
            // 
            // yMove
            // 
            this.yMove.Location = new System.Drawing.Point(60, 325);
            this.yMove.Name = "yMove";
            this.yMove.Size = new System.Drawing.Size(42, 20);
            this.yMove.TabIndex = 15;
            this.yMove.Text = "0";
            // 
            // zMove
            // 
            this.zMove.Location = new System.Drawing.Point(108, 325);
            this.zMove.Name = "zMove";
            this.zMove.Size = new System.Drawing.Size(42, 20);
            this.zMove.TabIndex = 16;
            this.zMove.Text = "0";
            // 
            // moveButton
            // 
            this.moveButton.Location = new System.Drawing.Point(12, 351);
            this.moveButton.Name = "moveButton";
            this.moveButton.Size = new System.Drawing.Size(138, 23);
            this.moveButton.TabIndex = 17;
            this.moveButton.Text = "Move";
            this.moveButton.UseVisualStyleBackColor = true;
            this.moveButton.Click += new System.EventHandler(this.moveButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 435);
            this.Controls.Add(this.moveButton);
            this.Controls.Add(this.zMove);
            this.Controls.Add(this.yMove);
            this.Controls.Add(this.xMove);
            this.Controls.Add(this.reflectZButton);
            this.Controls.Add(this.reflectYButton);
            this.Controls.Add(this.reflectXButton);
            this.Controls.Add(this.zoomButton);
            this.Controls.Add(this.zZoom);
            this.Controls.Add(this.yZoom);
            this.Controls.Add(this.xZoom);
            this.Controls.Add(this.rotateZ);
            this.Controls.Add(this.rotateY);
            this.Controls.Add(this.rotateX);
            this.Controls.Add(this.degreesSet);
            this.Controls.Add(this.drawFigure);
            this.Controls.Add(this.drawCoordPlates);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button drawCoordPlates;
        private System.Windows.Forms.Button drawFigure;
        private System.Windows.Forms.TextBox degreesSet;
        private System.Windows.Forms.Button rotateX;
        private System.Windows.Forms.Button rotateY;
        private System.Windows.Forms.Button rotateZ;
        private System.Windows.Forms.TextBox xZoom;
        private System.Windows.Forms.TextBox yZoom;
        private System.Windows.Forms.TextBox zZoom;
        private System.Windows.Forms.Button zoomButton;
        private System.Windows.Forms.Button reflectXButton;
        private System.Windows.Forms.Button reflectYButton;
        private System.Windows.Forms.Button reflectZButton;
        private System.Windows.Forms.TextBox xMove;
        private System.Windows.Forms.TextBox yMove;
        private System.Windows.Forms.TextBox zMove;
        private System.Windows.Forms.Button moveButton;
    }
}

