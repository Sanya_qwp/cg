﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.OpenGl;
using Tao.FreeGlut;
using Tao.Platform.Windows;
namespace practice5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            holst.InitializeContexts();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Glut.glutInit();
        }

        private void holst_Paint(object sender, PaintEventArgs e)
        {
            Gl.glViewport(0, 0, holst.Width, holst.Height);
            Gl.glClearColor(0f, 0f, 1f, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
            holst.Invalidate();
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
            Gl.glBegin(Gl.GL_TRIANGLES);
                Gl.glColor3f(1, 0, 0);
                Gl.glVertex3f(-0.5f, 0.5f, 0);
                Gl.glColor3f(0, 1, 0);
                Gl.glVertex3f(0.5f, 0.5f, 0);
                Gl.glColor3f(0, 0, 1);
                Gl.glVertex3f(0, -0.5f, 0);
            Gl.glEnd();
        }
    }
}
