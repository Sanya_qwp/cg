﻿using System;
using System.Windows.Forms;
using Tao.FreeGlut;
using Tao.OpenGl;
namespace lab7{
    public partial class Form1 : Form{
        int a = 0, x = 0, y = 1, z = 1;
        public Form1(){ InitializeComponent(); holst.InitializeContexts(); }
        private void holst_Load(object sender, EventArgs e){
            Glut.glutInit();
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE | Glut.GLUT_DEPTH);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            Gl.glLoadIdentity();
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();
        }
        private void timer1_Tick(object sender, EventArgs e){
            float[] fogColor = { 0.5f, 0.5f, 0.5f, 1 }; Gl.glEnable(Gl.GL_FOG);{
                Gl.glFogi(Gl.GL_FOG_MODE, Gl.GL_EXP);
                Gl.glFogfv(Gl.GL_FOG_COLOR, fogColor);
                Gl.glFogf(Gl.GL_FOG_DENSITY, 10);
                Gl.glFogf(Gl.GL_FOG_START, 4);
            }Gl.glFogf(Gl.GL_FOG_END, 8);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT); Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glClearColor(0, 0, 0, 1);
            Gl.glPushMatrix();{
                //Gl.glLoadIdentity();
                Gl.glRotatef(a += 1, x, y, z);
                Gl.glScalef(0.5f, 0.5f, 0.5f); Gl.glTranslated(-1.5f, 0, 0);{
                    Gl.glColor3f(1, 0.2f, 0.2f); Glut.glutSolidTorus(0.2f, 0.3f, 4, 8);    
                }
                Gl.glEnable(Gl.GL_ALPHA_TEST); Gl.glEnable(Gl.GL_BLEND);
                Gl.glBlendFunc(Gl.GL_ONE, Gl.GL_ONE);{
                    Gl.glColor3f(0.2f, 0.2f, 1); Glut.glutSolidOctahedron();
                } Gl.glDisable(Gl.GL_BLEND);
            }Gl.glPopMatrix();
            Gl.glPushMatrix();{
                Gl.glRotatef(90, 0, 1, 1);
                Gl.glScalef(1, 1, 1); Gl.glColor3f(0.2f, 1f, 0.2f); Glut.glutSolidCylinder(0.2f, 0.5f, 20, 20);
            }Gl.glPopMatrix();
            holst.Invalidate();
        }
        private void button2_Click(object sender, EventArgs e){
            if (timer1.Enabled == true){ timer1.Enabled = false; }
            else { timer1.Enabled = true; }
        }
    }
}