﻿using System;
using System.Windows.Forms;
using Tao.OpenGl;
using Tao.FreeGlut;
/*
 * Создать в окне трехмерную сцену, состоящую из шестиугольной проволочной пирамиды
 * и закрашенного куба.Расположить и окрасить в различные цвета по своему усмотрению.
 * Вращать объекты по таймеру (с помощью клавиш управления курсором).
*/
namespace lab6{
    public partial class Form1 : Form {
        int a = 1, x = 1, y = 0, z = 1;
        public Form1(){
            InitializeComponent();
            holst.InitializeContexts();
        }
        private void Form1_Load(object sender, EventArgs e){
            Gl.glViewport(0, 0, holst.Width, holst.Height); Gl.glClearColor(0f, 0f, 0f, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT); Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);
            holst.Invalidate();
            //timer1.Enabled = true;
            Gl.glScalef(0.5f, 0.5f, 0.5f);
            Glut.glutInit();
            Glut.glutWireDodecahedron();
        }
        private void holst_KeyDown(object sender, KeyEventArgs e){
            if (e.KeyValue == 87){ //W 
                x = 1; y = 0; z = 0;
            }
            if (e.KeyValue == 83) {//S
                x = -1; y = 0; z = 0;
            }
            if (e.KeyValue == 65) {//A
                x = 0; y = 1; z = 0;
            }
            if (e.KeyValue == 68){ //D
                x = 0; y = -1; z = 0;
            }
        }
        private void timer1_Tick(object sender, EventArgs e) {
            Gl.glEnable(Gl.GL_DEPTH_TEST); Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(0f, 0f, 0f, 1); Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
            Gl.glLoadIdentity(); Gl.glScalef(0.5f, 0.5f, 0.5f); holst.Invalidate();
            a += 2; Gl.glRotatef(a, x, y, z);
            Gl.glColor3f(0, 1, 1); Gl.glLineWidth(3);
            Gl.glBegin(Gl.GL_LINE_LOOP); {
                Gl.glVertex3f(-1f, -0.9f, -0.5f);
                Gl.glVertex3f(-0.5f, -0.9f, 0);
                Gl.glVertex3f(0.5f, -0.9f, 0);
                Gl.glVertex3f(1, -0.9f, -0.5f);
                Gl.glVertex3f(0.5f, -0.9f, -1);
                Gl.glVertex3f(-0.5f, -0.9f, -1);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_LINE_STRIP); {
                Gl.glVertex3f(-1, -0.9f, -0.5f);
                Gl.glVertex3f(0, 0.9f, -0.5f);   //Левая линия
                Gl.glVertex3f(-0.5f, -0.9f, 0);  //1 линия центр фронт
                Gl.glVertex3f(0, 0.9f, -0.5f);
                Gl.glVertex3f(-0.5f, -0.9f, -1); //1 линия центр бек
                Gl.glVertex3f(0, 0.9f, -0.5f);
                Gl.glVertex3f(0.5f, -0.9f, 0);   //2 линия центр фронт
                Gl.glVertex3f(0, 0.9f, -0.5f);
                Gl.glVertex3f(0.5f, -0.9f, -1); //2 линия центр бек
                Gl.glVertex3f(0, 0.9f, -0.5f);
                Gl.glVertex3f(1, -0.9f, -0.5f);  //Правая линия
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //задняя
                Gl.glColor3f(1, 0, 0);
                Gl.glVertex3f(-0.3f, -0.9f, -0.75f);
                Gl.glVertex3f(0.3f, -0.9f, -0.75f);
                Gl.glVertex3f(0.3f, -0.4f, -0.75f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.75f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //передняя
                Gl.glColor3f(1, 0, 1);
                Gl.glVertex3f(-0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(0.3f, -0.4f, -0.25f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //нижняя
                Gl.glColor3f(0, 1, 0);
                Gl.glVertex3f(-0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(0.3f, -0.9f, -0.75f);
                Gl.glVertex3f(-0.3f, -0.9f, -0.75f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //верхняя
                Gl.glColor3f(0, 0, 0.5f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.25f);
                Gl.glVertex3f(0.3f, -0.4f, -0.25f);
                Gl.glVertex3f(0.3f, -0.4f, -0.75f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.75f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //левая
                Gl.glColor3f(0, 0, 1);
                Gl.glVertex3f(-0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(-0.3f, -0.9f, -0.75f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.75f);
                Gl.glVertex3f(-0.3f, -0.4f, -0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS); { //правая
                Gl.glColor3f(1, 1, 1);
                Gl.glVertex3f(0.3f, -0.9f, -0.25f);
                Gl.glVertex3f(0.3f, -0.9f, -0.75f);
                Gl.glVertex3f(0.3f, -0.4f, -0.75f);
                Gl.glVertex3f(0.3f, -0.4f, -0.25f);
                
                
            } Gl.glEnd();
            holst.Invalidate();
        }
    }
}