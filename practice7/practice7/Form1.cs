﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.OpenGl;
namespace practice7{
    public partial class Form1 : Form{
        int a = 0, x = 1, y = 1, z = 0; float[] color_am = { 0, 0, 1 };
        private void holst_KeyDown(object sender, KeyEventArgs e){
            if (e.KeyCode == Keys.Escape) { Application.Exit(); }
            if (e.KeyValue == 87){ //W
                x = 1; y = 0; z = 0;
            }
            if (e.KeyValue == 83){ //S
                x = -1; y = 0; z = 0;
            }
            if (e.KeyValue == 65){ //A
                x = 0; y = 1; z = 1;
            }
            if (e.KeyValue == 68){ //D
                x = 1; y = 0; z = 1;
            }
        }
        private void xToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);
            Gl.glEnable(Gl.GL_NORMALIZE);
            Gl.glLightModelf(Gl.GL_LIGHT_MODEL_TWO_SIDE, Gl.GL_TRUE);
            float[] light0_dif = { 0.7f, 0.2f, 0.2f };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, light0_dif);
            float[] light0_pos = { 1, 0, 0, 0 };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light0_pos);
        }
        private void yToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);
            Gl.glEnable(Gl.GL_NORMALIZE);
            Gl.glLightModelf(Gl.GL_LIGHT_MODEL_TWO_SIDE, Gl.GL_TRUE);
            float[] light0_dif = { 0.7f, 0.2f, 0.2f };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, light0_dif);
            float[] light0_pos = { 0, 1, 0, 0 };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light0_pos);
        }
        private void zToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);
            Gl.glEnable(Gl.GL_NORMALIZE);
            Gl.glLightModelf(Gl.GL_LIGHT_MODEL_TWO_SIDE, Gl.GL_TRUE);
            float[] light0_dif = { 0.7f, 0.2f, 0.2f };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_DIFFUSE, light0_dif);
            float[] light0_pos = { 0, 0, 1, 0 };
            Gl.glLightfv(Gl.GL_LIGHT0, Gl.GL_POSITION, light0_pos);
        }
        private void ambientToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glMaterialfv(Gl.GL_FRONT_AND_BACK, Gl.GL_AMBIENT, color_am);
        }
        private void diffuseToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glMaterialfv(Gl.GL_FRONT_AND_BACK, Gl.GL_DIFFUSE, color_am);
        }
        private void specularToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glMaterialfv(Gl.GL_FRONT_AND_BACK, Gl.GL_SPECULAR, color_am);
        }
        private void emissionToolStripMenuItem_Click(object sender, EventArgs e){
            Gl.glMaterialfv(Gl.GL_FRONT_AND_BACK, Gl.GL_EMISSION, color_am);
        }
        public Form1(){
            InitializeComponent();
            holst.InitializeContexts();
        }
        private void Form1_Load(object sender, EventArgs e){
            Gl.glViewport(0, 0, holst.Width, holst.Height);
            Gl.glClearColor(0f, 0f, 0f, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
            Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);
            holst.Invalidate();
            timer.Enabled = true;
        }
        private void timer_Tick(object sender, EventArgs e){
            Gl.glClearColor(0, 0, 0, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glPushMatrix();
            Gl.glLoadIdentity();
            Gl.glRotatef(a += 2, x, y, z);
            Gl.glBegin(Gl.GL_QUADS);{ //Нижняя грань
                Gl.glNormal3f(0, -1, 0);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
            } Gl.glEnd(); holst.Invalidate();
            Gl.glBegin(Gl.GL_QUADS);{ //Верхняя грань
                Gl.glNormal3f(0, 1, 0);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, 0.5f, 0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, 0.5f, 0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, 0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, 0.5f, -0.5f);
            } Gl.glEnd(); holst.Invalidate();
            Gl.glBegin(Gl.GL_QUADS);{ //Левая грань
                Gl.glNormal3f(-1, 0, 0);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(-0.5f, 0.5f, 0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(-0.5f, 0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
            } Gl.glEnd(); holst.Invalidate();
            Gl.glBegin(Gl.GL_QUADS);{ //Правая грань
                Gl.glNormal3f(1, 0, 0);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, 0.5f, 0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, 0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
            } Gl.glEnd(); holst.Invalidate();
            Gl.glBegin(Gl.GL_QUADS); { //Передняя грань
                Gl.glNormal3f(0, 0, -1);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, 0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, 0.5f, 0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, 0.5f, 0.5f);
            } Gl.glEnd(); holst.Invalidate();
            Gl.glBegin(Gl.GL_QUADS);{ //Задняя грань
                Gl.glNormal3f(0, 0, 1);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, 0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, 0.5f, -0.5f);
            } Gl.glEnd(); holst.Invalidate();
        }
    }
}
