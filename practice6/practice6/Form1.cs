﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.FreeGlut;
using Tao.OpenGl;
using Tao.Platform.Windows;
namespace practice6
{
    
    public partial class Form1 : Form
        
    {
        int a = 0, x = 1, y = 1, z = 0;
        public Form1()
        {
            InitializeComponent();
            holst.InitializeContexts();
        }

        private void holst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape){ Application.Exit(); }
            if (e.KeyCode == Keys.Enter){
                Gl.glViewport(0, 0, holst.Width, holst.Height);
                Gl.glClearColor(0f, 0f, 0f, 1);
                Gl.glClear(Gl.GL_COLOR_BUFFER_BIT);
                Gl.glClear(Gl.GL_DEPTH_BUFFER_BIT);
                holst.Invalidate();
                
                holst.Invalidate();
                timer1.Enabled = true;
            }
            if (e.KeyValue == 87) //W
            {
                x = 1; y = 0; z = 0;
            }
            if (e.KeyValue == 83) //S
            {
                x = -1; y = 0; z = 0; 
            }
            if (e.KeyValue == 65) //A
            {
                x = 0; y = 1; z = 1;
            }
            if (e.KeyValue == 68) //D
            {
                x = 1; y = 0; z = 1;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Gl.glClearColor(0, 0, 0, 1);
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glPushMatrix();
            Gl.glLoadIdentity();
            a += 2;
            Gl.glRotatef(a, x, y, z);
            Gl.glBegin(Gl.GL_TRIANGLES);{ //Передняя грань
                Gl.glColor3f(1, 0, 0); Gl.glVertex3f(-0.5f, -0.5f, 0f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, 0f);
                Gl.glColor3f(0.5f, 0, 1); Gl.glVertex3f(0f, 0.5f, 0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_TRIANGLES);{ //Левая грань
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, 0f);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
                Gl.glColor3f(1, 0, 0.5f); Gl.glVertex3f(0f, 0.5f, 0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_TRIANGLES);{//Правая грань
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, 0f);
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
                Gl.glColor3f(1, 0, 1); Gl.glVertex3f(0f, 0.5f, 0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_TRIANGLES);{ //Задняя грань
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0f, 0.5f, 0.25f);
            } Gl.glEnd();
            Gl.glBegin(Gl.GL_QUADS);{ //Подложка
                Gl.glColor3f(0, 1, 0); Gl.glVertex3f(-0.5f, -0.5f, 0f);
                Gl.glColor3f(0, 0, 1); Gl.glVertex3f(0.5f, -0.5f, 0f);
                Gl.glColor3f(0, 1, 1); Gl.glVertex3f(0.5f, -0.5f, -0.5f);
                Gl.glColor3f(0, 0, 0); Gl.glVertex3f(-0.5f, -0.5f, -0.5f);
            } Gl.glEnd();
            holst.Invalidate();
        }
    }
}
